"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.processRenderFunctions = processRenderFunctions;
exports.componentAsPrioritisedFunction = componentAsPrioritisedFunction;
exports.inPriorityOrder = inPriorityOrder;
exports.default = exports.widgets = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactJss = _interopRequireDefault(require("react-jss"));

var _eventemitter = require("eventemitter2");

var _typeExtractor = _interopRequireDefault(require("./type-extractor"));

var _reactstrap = require("reactstrap");

require("bootstrap/dist/css/bootstrap.min.css");

var _classnames = _interopRequireDefault(require("classnames"));

var _some = _interopRequireDefault(require("lodash/some"));

var _widgetRow = _interopRequireDefault(require("./widget-row"));

var _shortid = _interopRequireDefault(require("shortid"));

var _fi = require("react-icons/fi");

var _contexts = require("./contexts");

var _swallowClicks = _interopRequireDefault(require("./swallow-clicks"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function noop() {}

var widgets = new _eventemitter.EventEmitter2({
  wildcard: true
});
exports.widgets = widgets;
var editorButton = {
  cursor: "pointer",
  padding: {
    left: "1em",
    right: "1em",
    top: "0.7em",
    bottom: "0.7em"
  },
  background: "whitesmoke",
  color: "gray",
  position: "absolute",
  borderRadius: "4px 4px 0 0",
  right: "1em"
};
var styles = {
  holder: {
    display: "flex",
    height: "100%",
    position: "relative",
    overflow: "hidden"
  },
  contextButton: {
    cursor: "pointer",
    padding: {
      left: "0.8em",
      right: "0.8em",
      top: "0.5em",
      bottom: "0.5em"
    },
    fontSize: "larger",
    fontWeight: "bold",
    background: "whitesmoke",
    color: "gray",
    position: "absolute",
    borderRadius: "4px 0px 0px 4px",
    right: 0,
    top: "20%",
    opacity: 0.76
  },
  tab: {
    cursor: "pointer"
  },
  widgets: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    minHeight: "100%",
    overflowY: "auto",
    "& *": {
      userSelect: "none"
    },
    "& input, & textarea": {
      userSelect: "initial"
    }
  },
  header: {
    background: "whitesmoke",
    paddingTop: 8,
    paddingBottom: 8,
    flexShrink: 0
  },
  left: {
    background: "darkgray",
    color: "whitesmoke",
    alignContent: "flex-start"
  },
  content: {},
  right: {
    background: "#555",
    color: "whitesmoke",
    height: "100%",
    alignContent: "flex-start"
  },
  body: {
    margin: {
      left: -15,
      right: -15
    },
    flexGrow: 1
  },
  spacer: {
    height: "40vh"
  },
  footer: {
    flexShrink: 0
  },
  editor: {
    boxShadow: {
      blur: 20
    },
    position: "absolute",
    bottom: 0,
    background: "white",
    border: "none",
    minHeight: "32px",
    left: 0,
    right: 0,
    zIndex: 5
  },
  editorContent: {
    maxHeight: "50vh",
    overflowY: "auto",
    background: "white"
  },
  editorInner: {
    position: "relative",
    background: "whitesmoke",
    paddingTop: 4,
    width: "100%"
  },
  editorButton: Object.assign({
    opacity: 0.6,
    bottom: 0
  }, editorButton),
  activeEditorButton: Object.assign({
    top: 1,
    transform: "translateY(-100%)",
    opacity: 1,
    boxShadow: {
      blur: 6,
      y: -6
    },
    zIndex: 4
  }, editorButton)
};

function inPriorityOrder(a, b) {
  return (a.priority || 1) - (b.priority || 1);
}

function componentAsPrioritisedFunction(Component, priority, callBeforeRender) {
  var output = function output(props) {
    return _react.default.createElement(Component, props);
  };

  output.priority = priority;
  output.callBeforeRender = callBeforeRender;
  return output;
}

function processRenderFunctions(list) {
  list.sort(inPriorityOrder).filter(function (item) {
    return item.callBeforeRender;
  }).forEach(function (item) {
    return item.callBeforeRender(list);
  });
  return list;
}

function hasEditorsInstalled(_ref) {
  var header = _ref.header,
      tabs = _ref.tabs;
  return header.length > 0 || (0, _some.default)(tabs, function (tab) {
    return tab.content.length > 0;
  });
}

function calculateBreakpoint(width) {
  if (width < 576) return "xs";
  if (width < 768) return "sm";
  if (width < 992) return "md";
  if (width < 1200) return "lg";
  return "xl";
}

function Widgets(_ref2) {
  var classes = _ref2.classes,
      style = _ref2.style,
      document = _ref2.document,
      design = _ref2.design,
      type = _ref2.type,
      editable = _ref2.editable,
      _ref2$editorMinHeight = _ref2.editorMinHeight,
      editorMinHeight = _ref2$editorMinHeight === void 0 ? 0.3 : _ref2$editorMinHeight,
      _ref2$editorMaxHeight = _ref2.editorMaxHeight,
      editorMaxHeight = _ref2$editorMaxHeight === void 0 ? 0.3 : _ref2$editorMaxHeight,
      _ref2$updated = _ref2.updated,
      updated = _ref2$updated === void 0 ? noop : _ref2$updated,
      props = _objectWithoutProperties(_ref2, ["classes", "style", "document", "design", "type", "editable", "editorMinHeight", "editorMaxHeight", "updated"]);

  if (!document || !design) {
    return _react.default.createElement("div", null, "You must set 'document' and 'design' parameters");
  }

  type = (0, _typeExtractor.default)(document, design, {
    type: type
  });

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      editMode = _useState2[0],
      setEditMode = _useState2[1];

  var _useState3 = (0, _react.useState)(calculateBreakpoint(window.width)),
      _useState4 = _slicedToArray(_useState3, 2),
      size = _useState4[0],
      setSize = _useState4[1];

  if (editMode && !editable) setEditMode(false);

  var _useState5 = (0, _react.useState)(design),
      _useState6 = _slicedToArray(_useState5, 2),
      focus = _useState6[0],
      setReactFocus = _useState6[1];

  var _useState7 = (0, _react.useState)("general"),
      _useState8 = _slicedToArray(_useState7, 2),
      currentTab = _useState8[0],
      setTab = _useState8[1];

  var _useState9 = (0, _react.useState)({}),
      _useState10 = _slicedToArray(_useState9, 1),
      myBag = _useState10[0];

  var _useState11 = (0, _react.useState)(false),
      _useState12 = _slicedToArray(_useState11, 2),
      contextOpen = _useState12[0],
      setContextOpen = _useState12[1];

  var _useState13 = (0, _react.useState)(_shortid.default.generate()),
      _useState14 = _slicedToArray(_useState13, 1),
      id = _useState14[0];

  var _useState15 = (0, _react.useState)("update"),
      _useState16 = _slicedToArray(_useState15, 2),
      doReactUpdate = _useState16[1];

  var _useState17 = (0, _react.useState)(0),
      _useState18 = _slicedToArray(_useState17, 2),
      height = _useState18[0],
      setHeight = _useState18[1];

  function setFocus(newFocus) {
    if (!(0, _isEqual.default)(focus, newFocus)) {
      setReactFocus(newFocus);
      focus = newFocus;
    }
  }

  function changed() {
    updated({
      document: document,
      design: design
    });
  }

  function doUpdate() {
    doReactUpdate();
    changed();
  }

  var queueUpdate = (0, _debounce.default)(doUpdate, 1000, {
    leading: false,
    trailing: true
  });
  var ref = (0, _react.useRef)(null);
  (0, _react.useEffect)(function () {
    var newHeight = ref.current.offsetHeight;
    if (newHeight !== height) setHeight(newHeight);
    var newSize = calculateBreakpoint(ref.current.offsetWidth);
    if (newSize !== size) setSize(newSize);
    window.addEventListener('resize', queueUpdate);
    return function () {
      window.removeEventListener('resize', queueUpdate);
    };
  });
  var minHeight = height;
  var maxHeight = height;
  /* Calculate the editor height */

  editorMinHeight = Array.isArray(editorMinHeight) ? editorMinHeight : [editorMinHeight];
  editorMaxHeight = Array.isArray(editorMaxHeight) ? editorMaxHeight : [editorMaxHeight];
  editorMinHeight.forEach(function (h) {
    return h < 1 ? minHeight = Math.min(minHeight, height * h) : minHeight = Math.max(minHeight, h);
  });
  editorMaxHeight.forEach(function (h) {
    return h < 1 ? maxHeight = Math.min(maxHeight, height * h) : maxHeight = Math.max(maxHeight, h);
  });
  var layout = {
    headerLeft: [],
    headerCentre: [],
    headerRight: [],
    left: [],
    content: [],
    right: [],
    footer: [],
    context: []
  };
  var editorLayout = {
    header: [],
    tabs: {
      general: {
        title: _react.default.createElement(_fi.FiSettings, null),
        content: []
      }
    },
    focus: focus
  };

  var _useContext = (0, _react.useContext)(_contexts.DocumentContext),
      _useContext$root = _useContext.root,
      root = _useContext$root === void 0 ? design : _useContext$root,
      _useContext$bag = _useContext.bag,
      bag = _useContext$bag === void 0 ? myBag : _useContext$bag;

  var editorContext = (0, _react.useContext)(_contexts.EditorContext) || editorLayout;
  var context = {
    size: size,
    root: root,
    design: design,
    document: document,
    layout: layout,
    editor: editorContext,
    focus: focus,
    setFocus: setFocus,
    changed: changed,
    doUpdate: doUpdate,
    queueUpdate: queueUpdate,
    bag: bag,
    editMode: editMode
  };
  widgets.emit("edit.".concat(type), context);
  props.editMode = editMode;

  if (!editorContext.tabs[currentTab]) {
    currentTab = "general";
  }

  if (layout.context.length === 0 && contextOpen) {
    setContextOpen(false);
  }

  return _react.default.createElement(_contexts.EditorContext.Provider, {
    value: editorContext
  }, _react.default.createElement(_contexts.EditModeContext.Provider, {
    value: editMode
  }, _react.default.createElement(_contexts.DocumentContext.Provider, {
    value: context
  }, _react.default.createElement("div", {
    className: classes.holder,
    style: style,
    ref: ref
  }, _react.default.createElement(_reactstrap.Container, {
    onClick: function onClick() {
      return setFocus(design);
    },
    fluid: true,
    className: classes.widgets
  },
  /* The Header */
  (0, _widgetRow.default)(_objectSpread({
    colType: "xs",
    className: props.headerClass || classes.header,
    left: layout.headerLeft,
    right: layout.headerRight,
    centre: layout.headerCentre,
    props: props
  }, context)),
  /* The Centre */
  (0, _widgetRow.default)(_objectSpread({
    style: {
      paddingBottom: editMode ? "50vh" : 0
    },
    className: props.className || classes.body,
    leftClass: props.leftClass || classes.left,
    rightClass: props.rightClass || classes.right,
    centreClass: props.contentClass || classes.content,
    left: layout.left,
    right: layout.right,
    centre: layout.content,
    props: props
  }, context)),
  /* The Footer */
  (0, _widgetRow.default)(_objectSpread({
    className: props.footerClass || classes.footer,
    centre: layout.footer,
    props: props
  }, context)),
  /* The Context Menu */
  _react.default.createElement(_swallowClicks.default, {
    style: {
      display: layout.context.length > 0 ? "initial" : "none"
    }
  }, _react.default.createElement("div", {
    className: classes.contextButton,
    id: "context".concat(id),
    onClick: function onClick() {
      return setContextOpen(!contextOpen);
    }
  }, props.contextMenu || _react.default.createElement(_fi.FiMenu, null)), _react.default.createElement(_reactstrap.Popover, {
    placement: "left",
    isOpen: contextOpen,
    target: "context".concat(id),
    toggle: function toggle() {
      return setContextOpen(!contextOpen);
    }
  }, _react.default.createElement(_reactstrap.PopoverBody, null, layout.context.map(function (item, index) {
    return _react.default.createElement("div", {
      key: index
    }, item(context));
  })))),
  /* The editor */
  editorLayout === editorContext && editable && (editMode ? _react.default.createElement("div", {
    onClick: function onClick(event) {
      return event.stopPropagation();
    },
    className: classes.editor
  }, _react.default.createElement("div", {
    className: classes.editorInner
  }, _react.default.createElement("div", {
    onClick: function onClick() {
      return setEditMode(!editMode);
    },
    className: classes.activeEditorButton
  }, _react.default.createElement(_fi.FiX, null)), _react.default.createElement(_reactstrap.Nav, {
    tabs: true
  }, Object.entries(editorLayout.tabs).sort(inPriorityOrder).map(function (_ref3) {
    var _ref4 = _slicedToArray(_ref3, 2),
        key = _ref4[0],
        tab = _ref4[1];

    return _react.default.createElement(_reactstrap.NavItem, {
      className: classes.tab,
      key: key
    }, _react.default.createElement(_reactstrap.NavLink, {
      className: (0, _classnames.default)({
        active: currentTab === key
      }),
      onClick: function onClick() {
        return setTab(key);
      }
    }, tab.title));
  })), _react.default.createElement(_reactstrap.TabContent, {
    style: {
      maxHeight: maxHeight,
      minHeight: minHeight
    },
    className: classes.editorContent,
    activeTab: currentTab
  }, Object.entries(editorLayout.tabs).map(function (_ref5) {
    var _ref6 = _slicedToArray(_ref5, 2),
        key = _ref6[0],
        tab = _ref6[1];

    return _react.default.createElement(_reactstrap.TabPane, {
      key: key,
      tabId: key
    }, _react.default.createElement(_reactstrap.Container, null, _react.default.createElement("br", null), processRenderFunctions(tab.content, context).map(function (child, key) {
      return _react.default.createElement("div", {
        key: key
      }, child(_objectSpread({}, context, {
        tab: tab
      })));
    })));
  })))) : hasEditorsInstalled(editorLayout) && _react.default.createElement("div", {
    className: classes.editorButton,
    onClick: function onClick() {
      return setEditMode(!editMode);
    }
  }, _react.default.createElement(_fi.FiSettings, null))))))));
}

var _default = (0, _reactJss.default)(styles)(Widgets);

exports.default = _default;