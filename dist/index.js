"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.arrayMoveInPlace = arrayMoveInPlace;
Object.defineProperty(exports, "Widgets", {
  enumerable: true,
  get: function get() {
    return _widgets.default;
  }
});
Object.defineProperty(exports, "inPriorityOrder", {
  enumerable: true,
  get: function get() {
    return _widgets.inPriorityOrder;
  }
});
Object.defineProperty(exports, "widgets", {
  enumerable: true,
  get: function get() {
    return _widgets.widgets;
  }
});
Object.defineProperty(exports, "componentAsPrioritisedFunction", {
  enumerable: true,
  get: function get() {
    return _widgets.componentAsPrioritisedFunction;
  }
});
Object.defineProperty(exports, "Widget", {
  enumerable: true,
  get: function get() {
    return _widgetComponent.Widget;
  }
});
Object.defineProperty(exports, "SortableWidget", {
  enumerable: true,
  get: function get() {
    return _widgetComponent.SortableWidget;
  }
});
Object.defineProperty(exports, "SortableWidgetContainer", {
  enumerable: true,
  get: function get() {
    return _widgetComponent.SortableWidgetContainer;
  }
});
Object.defineProperty(exports, "DocumentContext", {
  enumerable: true,
  get: function get() {
    return _contexts.DocumentContext;
  }
});
Object.defineProperty(exports, "EditModeContext", {
  enumerable: true,
  get: function get() {
    return _contexts.EditModeContext;
  }
});
Object.defineProperty(exports, "EditorContext", {
  enumerable: true,
  get: function get() {
    return _contexts.EditorContext;
  }
});
Object.defineProperty(exports, "useStateFromValueAndForward", {
  enumerable: true,
  get: function get() {
    return _useStateAndForward.useStateFromValueAndForward;
  }
});
exports.default = void 0;

var _widgets = _interopRequireWildcard(require("./widgets"));

var _widgetComponent = require("./widget-component");

var _contexts = require("./contexts");

var _useStateAndForward = require("./use-state-and-forward");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function arrayMoveInPlace(array, previousIndex, newIndex) {
  if (newIndex >= array.length) {
    var k = newIndex - array.length;

    while (k-- + 1) {
      array.push(undefined);
    }
  }

  array.splice(newIndex, 0, array.splice(previousIndex, 1)[0]);
}

var _default = _widgets.default;
exports.default = _default;