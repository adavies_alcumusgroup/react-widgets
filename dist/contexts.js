"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EditorContext = exports.DocumentContext = exports.EditModeContext = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EditModeContext = _react.default.createContext(false);

exports.EditModeContext = EditModeContext;

var DocumentContext = _react.default.createContext({
  design: null,
  document: null
});

exports.DocumentContext = DocumentContext;

var EditorContext = _react.default.createContext(null);

exports.EditorContext = EditorContext;