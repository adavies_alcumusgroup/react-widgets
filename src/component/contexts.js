import React from "react"

const EditModeContext = React.createContext(false)
const DocumentContext = React.createContext({design: null, document: null})
const EditorContext = React.createContext(null)


export {EditModeContext, DocumentContext, EditorContext}
