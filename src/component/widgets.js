import React, {useContext, useEffect, useRef, useState} from "react"
import injectSheet from "react-jss"
import {EventEmitter2 as Events} from "eventemitter2"
import extractType from "./type-extractor"
import {Container, Nav, NavItem, NavLink, Popover, PopoverBody, TabContent, TabPane} from "reactstrap"
import 'bootstrap/dist/css/bootstrap.min.css'
import classnames from "classnames"
import some from "lodash/some"
import layoutRow from "./widget-row"
import shortid from "shortid"
import {FiMenu, FiSettings, FiX} from "react-icons/fi"
import {DocumentContext, EditModeContext, EditorContext} from "./contexts"
import SwallowClicks from "./swallow-clicks"
import debounce from "lodash/debounce"
import isEqual from "lodash/isEqual"

function noop() {
}

const widgets = new Events({wildcard: true})
export {widgets}

const editorButton = {
    cursor: "pointer",
    padding: {
        left: "1em",
        right: "1em",
        top: "0.7em",
        bottom: "0.7em"
    },
    background: "whitesmoke",
    color: "gray",
    position: "absolute",
    borderRadius: "4px 4px 0 0",
    right: "1em"
}

const styles = {
    holder: {
        display: "flex",
        height: "100%",
        position: "relative",
        overflow: "hidden"
    },
    contextButton: {
        cursor: "pointer",
        padding: {
            left: "0.8em",
            right: "0.8em",
            top: "0.5em",
            bottom: "0.5em"
        },
        fontSize: "larger",
        fontWeight: "bold",
        background: "whitesmoke",
        color: "gray",
        position: "absolute",
        borderRadius: "4px 0px 0px 4px",
        right: 0,
        top: "20%",
        opacity: 0.76
    },
    tab: {
        cursor: "pointer"
    },
    widgets: {
        width: "100%",
        display: "flex",
        flexDirection: "column",
        minHeight: "100%",
        overflowY: "auto",
        "& *": {
            userSelect: "none"
        },
        "& input, & textarea": {
            userSelect: "initial"
        },
    },
    header: {
        background: "whitesmoke",
        paddingTop: 8,
        paddingBottom: 8,
        flexShrink: 0
    },
    left: {
        background: "darkgray",
        color: "whitesmoke",
        alignContent: "flex-start"
    },
    content: {},
    right: {
        background: "#555",
        color: "whitesmoke",
        height: "100%",
        alignContent: "flex-start"
    },
    body: {
        margin: {
            left: -15,
            right: -15
        },
        flexGrow: 1
    },
    spacer: {
        height: "40vh"
    },
    footer: {
        flexShrink: 0
    },
    editor: {
        boxShadow: {
            blur: 20
        },
        position: "absolute",
        bottom: 0,
        background: "white",
        border: "none",
        minHeight: "32px",
        left: 0,
        right: 0,
        zIndex: 5
    },
    editorContent: {
        maxHeight: "50vh",
        overflowY: "auto",
        background: "white"
    },
    editorInner: {
        position: "relative",
        background: "whitesmoke",
        paddingTop: 4,
        width: "100%"
    },
    editorButton: Object.assign({
        opacity: 0.6,
        bottom: 0
    }, editorButton),
    activeEditorButton: Object.assign({
        top: 1,
        transform: "translateY(-100%)",
        opacity: 1,
        boxShadow: {
            blur: 6,
            y: -6
        },
        zIndex: 4
    }, editorButton)
}

function inPriorityOrder(a, b) {
    return (a.priority || 1) - (b.priority || 1)
}

function componentAsPrioritisedFunction(Component, priority, callBeforeRender) {
    let output = (props) => <Component {...props}/>
    output.priority = priority
    output.callBeforeRender = callBeforeRender
    return output
}

function processRenderFunctions(list) {
    list.sort(inPriorityOrder).filter(item => item.callBeforeRender).forEach(item => item.callBeforeRender(list))
    return list
}

export {processRenderFunctions}
export {componentAsPrioritisedFunction}

export {inPriorityOrder}

function hasEditorsInstalled({header, tabs}) {
    return header.length > 0 || some(tabs, tab => tab.content.length > 0)
}

function calculateBreakpoint(width) {
    if (width < 576) return "xs"
    if (width < 768) return "sm"
    if (width < 992) return "md"
    if (width < 1200) return "lg"
    return "xl"
}

function Widgets({classes, style, document, design, type, editable, editorMinHeight = 0.3, editorMaxHeight = 0.3, updated = noop, ...props}) {
    if(!document || !design) {
        return <div>You must set 'document' and 'design' parameters</div>
    }
    type = extractType(document, design, {type})
    let [editMode, setEditMode] = useState(false)
    let [size, setSize] = useState(calculateBreakpoint(window.width))
    if (editMode && !editable) setEditMode(false)
    let [focus, setReactFocus] = useState(design)
    let [currentTab, setTab] = useState("general")
    let [myBag] = useState({})
    let [contextOpen, setContextOpen] = useState(false)
    let [id] = useState(shortid.generate())
    let [, doReactUpdate] = useState("update")
    let [height, setHeight] = useState(0)

    function setFocus(newFocus) {
        if (!isEqual(focus, newFocus)) {
            setReactFocus(newFocus)
            focus = newFocus
        }
    }

    function changed() {
        updated({document, design})
    }

    function doUpdate() {
        doReactUpdate()
        changed()
    }

    const queueUpdate = debounce(doUpdate, 1000, {leading: false, trailing: true})

    let ref = useRef(null)
    useEffect(() => {
        let newHeight = ref.current.offsetHeight
        if (newHeight !== height) setHeight(newHeight)
        let newSize = calculateBreakpoint(ref.current.offsetWidth)
        if (newSize !== size) setSize(newSize)
        window.addEventListener('resize', queueUpdate)
        return () => {
            window.removeEventListener('resize', queueUpdate)
        }
    })

    let minHeight = height
    let maxHeight = height

    /* Calculate the editor height */
    editorMinHeight = Array.isArray(editorMinHeight) ? editorMinHeight : [editorMinHeight]
    editorMaxHeight = Array.isArray(editorMaxHeight) ? editorMaxHeight : [editorMaxHeight]
    editorMinHeight.forEach(h => (h < 1) ? minHeight = Math.min(minHeight, height * h) : minHeight = Math.max(minHeight, h))
    editorMaxHeight.forEach(h => (h < 1) ? maxHeight = Math.min(maxHeight, height * h) : maxHeight = Math.max(maxHeight, h))

    let layout = {
        headerLeft: [],
        headerCentre: [],
        headerRight: [],
        left: [],
        content: [],
        right: [],
        footer: [],
        context: []
    }
    let editorLayout = {
        header: [],
        tabs: {
            general: {
                title: <FiSettings/>,
                content: []
            }
        },
        focus
    }
    let {root = design, bag = myBag} = useContext(DocumentContext)
    let editorContext = useContext(EditorContext) || editorLayout
    let context = {
        size,
        root,
        design,
        document,
        layout,
        editor: editorContext,
        focus,
        setFocus,
        changed,
        doUpdate,
        queueUpdate,
        bag,
        editMode
    }
    widgets.emit(`edit.${type}`, context)
    props.editMode = editMode
    if (!editorContext.tabs[currentTab]) {
        currentTab = "general"
    }
    if (layout.context.length === 0 && contextOpen) {
        setContextOpen(false)
    }
    return (
        <EditorContext.Provider value={editorContext}>
            <EditModeContext.Provider value={editMode}>
                <DocumentContext.Provider
                    value={context}>
                    <div className={classes.holder} style={style} ref={ref}>
                        <Container onClick={() => setFocus(design)} fluid={true} className={classes.widgets}>
                            {
                                /* The Header */
                                layoutRow({
                                    colType: "xs",
                                    className: props.headerClass || classes.header,
                                    left: layout.headerLeft,
                                    right: layout.headerRight,
                                    centre: layout.headerCentre,
                                    props,
                                    ...context
                                })
                            }
                            {
                                /* The Centre */
                                layoutRow({
                                    style: {paddingBottom: editMode ? "50vh" : 0},
                                    className: props.className || classes.body,
                                    leftClass: props.leftClass || classes.left,
                                    rightClass: props.rightClass || classes.right,
                                    centreClass: props.contentClass || classes.content,
                                    left: layout.left,
                                    right: layout.right,
                                    centre: layout.content,
                                    props,
                                    ...context
                                })
                            }
                            {
                                /* The Footer */
                                layoutRow({
                                    className: props.footerClass || classes.footer,
                                    centre: layout.footer,
                                    props,
                                    ...context
                                })
                            }
                            {
                                /* The Context Menu */
                                <SwallowClicks style={{display: layout.context.length > 0 ? "initial" : "none"}}>
                                    <div className={classes.contextButton} id={`context${id}`}
                                         onClick={() => setContextOpen(!contextOpen)}>
                                        {props.contextMenu || <FiMenu/>}
                                    </div>
                                    <Popover placement="left" isOpen={contextOpen} target={`context${id}`}
                                             toggle={() => setContextOpen(!contextOpen)}>
                                        <PopoverBody>
                                            {layout.context.map((item, index) => (
                                                <div key={index}>{item(context)}</div>))}
                                        </PopoverBody>
                                    </Popover>
                                </SwallowClicks>
                            }
                            {
                                /* The editor */
                                editorLayout === editorContext && editable && (
                                    editMode
                                        ? <div onClick={(event) => event.stopPropagation()} className={classes.editor}>
                                            <div className={classes.editorInner}>
                                                <div onClick={() => setEditMode(!editMode)}
                                                     className={classes.activeEditorButton}>
                                                    <FiX/>
                                                </div>
                                                <Nav tabs>
                                                    {Object.entries(editorLayout.tabs).sort(inPriorityOrder).map(([key, tab]) => {
                                                        return <NavItem className={classes.tab} key={key}>
                                                            <NavLink
                                                                className={classnames({active: currentTab === key})}
                                                                onClick={() => setTab(key)}>
                                                                {tab.title}
                                                            </NavLink>
                                                        </NavItem>
                                                    })}
                                                </Nav>
                                                <TabContent style={{maxHeight, minHeight}}
                                                            className={classes.editorContent} activeTab={currentTab}>
                                                    {Object.entries(editorLayout.tabs).map(([key, tab]) => {
                                                        return <TabPane key={key} tabId={key}>
                                                            <Container>
                                                                <br/>
                                                                {processRenderFunctions(tab.content, context).map((child, key) => {
                                                                    return <div key={key}>{child({...context, tab})}</div>
                                                                })}
                                                            </Container>

                                                        </TabPane>
                                                    })}
                                                </TabContent>
                                            </div>
                                        </div>
                                        : hasEditorsInstalled(editorLayout) &&
                                        <div className={classes.editorButton} onClick={() => setEditMode(!editMode)}>
                                            <FiSettings/>
                                        </div>
                                )
                            }
                        </Container>
                    </div>
                </DocumentContext.Provider>
            </EditModeContext.Provider>
        </EditorContext.Provider>
    )
}

export default injectSheet(styles)(Widgets)
