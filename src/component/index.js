import Widgets, {inPriorityOrder, widgets, componentAsPrioritisedFunction} from "./widgets"
import {Widget, SortableWidget, SortableWidgetContainer} from "./widget-component"
import {DocumentContext, EditModeContext, EditorContext} from "./contexts"
import {useStateFromValueAndForward} from "./use-state-and-forward"

function arrayMoveInPlace(array, previousIndex, newIndex) {
    if (newIndex >= array.length) {
        var k = newIndex - array.length;

        while (k-- + 1) {
            array.push(undefined);
        }
    }
    array.splice(newIndex, 0, array.splice(previousIndex, 1)[0]);
}

export {Widgets, Widget, SortableWidget, arrayMoveInPlace, SortableWidgetContainer, DocumentContext, EditorContext, EditModeContext, inPriorityOrder, widgets, useStateFromValueAndForward, componentAsPrioritisedFunction}

export default Widgets
