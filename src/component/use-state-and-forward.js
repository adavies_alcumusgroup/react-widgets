import {useState} from "react"

export function noop() {
}

function useStateFromValueAndForward(state, forward = noop) {
    const [value, set] = useState(state)
    return [value, (event) => {
        let v = event.target ? event.target.value : event
        forward(v)
        return set(v)
    }]
}

export {useStateFromValueAndForward}
