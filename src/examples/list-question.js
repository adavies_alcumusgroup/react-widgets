import React from "react"
import {componentAsPrioritisedFunction, useStateFromValueAndForward, widgets} from "../component/index"
import {createDefaultQuestion} from "./edit-questions"
import {Button, FormGroup, Input, Label} from "reactstrap"
import {FiActivity} from "react-icons/fi"
import injectSheet from "react-jss"


widgets.on("questions.types", types => {
    types.push({
        name: "Select",
        create: () => createDefaultQuestion({type: "list", question: "Select a value", answers: []}),
    })
})

const style = {
    input: {
        margin: {
            top: 8,
            bottom: 8,
        },
    },
}

const Render = injectSheet(style)(({readOnly, value, setValue, design, classes}) => {
    const [text, set] = useStateFromValueAndForward(value, setValue)
    if (!text && design.items && design.items.answers && design.items.answers.length) {
        set(design.items.answers[0].value)
    }
    design.item.setValue = set
    return (
        design.item.question ? <FormGroup key={design.item.id}>
                <Label dangerouslySetInnerHTML={{__html: design.item.question}}></Label>
                <Input readOnly={readOnly} key={design.item.id} type="select" value={text || ""}
                       onChange={set} placeholder={design.item.placeholder || ""}>
                    {design.item.answers.map((answer, index) => (
                        <option key={index} value={answer.value}>{answer.text || answer.value}</option>
                    ))}
                </Input>
            </FormGroup>
            : (
                <Input readOnly={readOnly} key={design.item.id} className={classes.input} type="select" value={text || ""}
                       onChange={set} placeholder={design.item.placeholder || ""}>
                    {design.item.answers.map((answer, index) => (
                        <option key={index} value={answer.value}>{answer.text || answer.value}</option>
                    ))}
                </Input>
            )
    )
})


widgets.on("editor.question.list", ({content, ok}) => {
    ok && content.push(componentAsPrioritisedFunction(Render))
})

function Answers(props) {
    const {focus: {selectedQuestion}, doUpdate, key} = props
    return <FormGroup>
        <Input type="textarea"
               key={key}
               rows={8}
               style={{resize: "none"}}
               placeholder="One entry per line. e.g. LABEL=value or just value"
               value={selectedQuestion.answers.map(answer => (answer.text ? answer.text + "=" + answer.value : answer.value || "")).join("\n")}
               onChange={(e) => {
                   selectedQuestion.answers = e.target.value.split("\n").map(l => l.trim()).map(line => {
                       let parts = line.split("=")
                       return parts.length > 1 ? {value: parts[1], text: parts[0]} : {
                           value: parts[0],
                           text: undefined,
                       }
                   })
                   doUpdate()
               }}/>
    </FormGroup>
}

function ListOfAnswers({focus: {selectedQuestion, answers}, design, document, doUpdate}) {
    return <Button color="primary" onClick={() => {
        let values = selectedQuestion.answers.filter(item => item.value !== answers[selectedQuestion.name])
        selectedQuestion.setValue(values[Math.floor(Math.random() * values.length)].value)
    }}>
        <FiActivity/> Choose At Random
    </Button>
}


widgets.on("edit.*", function ({editor, layout, focus}) {
    if (focus.selectedQuestion && focus.selectedQuestion.type === "list") {
        editor.tabs.answers = {
            title: "Possible Answers",
            content: [componentAsPrioritisedFunction(Answers)],
        }
        focus.selectedQuestion.answers.length && layout.context.push(componentAsPrioritisedFunction(ListOfAnswers))
    }
})
