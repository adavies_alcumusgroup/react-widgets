import React from "react"
import Switch from "@trendmicro/react-toggle-switch"
import '@trendmicro/react-toggle-switch/dist/react-toggle-switch.css'

import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {Col} from "reactstrap"
import injectSheet from "react-jss"

const styles = {}

const FilterPanel = injectSheet(styles)(function FilterPanel(props) {
    const {classes, document, changed, setReadOnly} = props
    return <Col xs={12} className={classes.column}>
        <Switch checked={!!document.readonly} size="lg" onChange={() => {
            document.readonly = !document.readonly
            setReadOnly(document.readonly)
            changed()
        }}/>
        Read only
    </Col>
})

widgets.on("edit.*", function ({design, layout}) {
    if (design.questions) {
        layout.right.push(componentAsPrioritisedFunction(FilterPanel))
    }
})

