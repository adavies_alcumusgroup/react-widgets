/* This is pretty much the whole logic for the questionnaire*/

import React from "react"
import {arrayMoveInPlace, SortableWidget, SortableWidgetContainer, widgets} from "../component/index"
import {Col} from "reactstrap"

function Questions({questions, editMode, answers, changed, doUpdate, focus: {selectedQuestions, selectedQuestion}, fallbackCaption, ...props}) {
    const questionContext = {questions, ...props, answers, changed}
    widgets.emit("filter.questions", questionContext)
    return <SortableWidgetContainer {...props} isSelected={selectedQuestions === questions}
                                    axis="y"
                                    distance={4}
                                    focusOn={{selectedQuestions: questions}}
                                    design={questions}
                                    onSortEnd={({oldIndex, newIndex})=>{
                                        arrayMoveInPlace(questions, oldIndex, newIndex)
                                        doUpdate()
                                    }}
                                    document={answers}
                                    type="questions">
        {
            questionContext.questions.length
                ? questionContext.questions.map((question, index) => (
                    <SortableWidget {...props} key={question.id || index}
                                    index={index}
                                    disabled={!editMode}
                                    design={{item: question, list: questions}}
                                    document={answers}
                                    value={answers[question.name]}
                                    setValue={(value) => {
                                        answers[question.name] = value
                                        changed()
                                    }}
                                    isSelected={selectedQuestion === question}
                                    focusOn={{
                                        selectedQuestions: questions,
                                        selectedQuestion: question,
                                        answers
                                    }}
                                    type={[`question.${question.type || 'default'}`, "list", "copyable"]}/>))
                : (
                    <div>
                        {fallbackCaption !== undefined ? fallbackCaption : "No questions..."}
                    </div>)
        }
    </SortableWidgetContainer>
}

const Render = ({design, document, ...props}) => {
    return <Col key={"questions-col"}>
        <Questions questions={design.questions} answers={document.answers} {...props}/>
    </Col>
}


widgets.on("edit.*", function ({design, document, layout}) {
    if (design.questions) {
        document.answers = document.answers || {}
        layout.content.push(Render)
    }
})


export {Questions}
