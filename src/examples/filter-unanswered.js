import React from "react"
import Switch from "@trendmicro/react-toggle-switch"
import '@trendmicro/react-toggle-switch/dist/react-toggle-switch.css'

import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {Col} from "reactstrap"
import injectSheet from "react-jss"

const styles = {}

const FilterPanel = injectSheet(styles)(function FilterPanel(props) {
    const {classes, bag, doUpdate} = props
    return <Col xs={12} className={classes.column}>
        <Switch checked={!!bag.unanswered} size="lg" onChange={() => {
            bag.unanswered = !bag.unanswered
            doUpdate()
        }}/>
        Unanswered only
    </Col>
})

widgets.on("filter.questions", function (context) {
    const {bag, answers, focus: {selectedQuestion} = {}} = context
    if (bag.unanswered) {
        context.questions = context.questions.filter(question => {
            return question === selectedQuestion || !(answers[question.name] || "").trim()
        })
    }
})

widgets.on("edit.*", function ({design, layout}) {
    if (design.questions) {
        layout.right.push(componentAsPrioritisedFunction(FilterPanel))
    }
})

