import injectSheet from "react-jss"
import {componentAsPrioritisedFunction, widgets} from "../component"
import React from "react"
import {Col, FormGroup, Input, Label} from "reactstrap"

const styles = {}


const SizeInfo = injectSheet(styles)(function SizeInfo({focus: {selectedWidget}, doUpdate}) {
    return (
        <>
            <FormGroup row>
                <Label sm={2}>Width</Label>
                <Col sm={10}>
                    <Input type="range" min={2} max={12} value={selectedWidget.width || 3} onChange={e => {
                        selectedWidget.width = e.target.value
                        doUpdate()
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Height</Label>
                <Col sm={10}>
                    <Input type="range" min={3} max={10} value={selectedWidget.height || 1} onChange={e => {
                        selectedWidget.height = e.target.value
                        doUpdate()
                    }}/>
                </Col>
            </FormGroup>
        </>
    )
})

widgets.on("edit.dashboard", function ({editor, focus: {selectedWidget}}) {
    if (selectedWidget) {
        editor.tabs.size = {title: "Size", content: [componentAsPrioritisedFunction(SizeInfo)]}
    }
})
