import React from "react"
import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {Col, Form, FormGroup, Input, Label} from "reactstrap"
import {heading} from "./styles"
import injectSheet from "react-jss"

const styles = {
    title: {
        fontWeight: "bold"
    },
    heading
}
const RenderTab = injectSheet(styles)(({doUpdate, design, classes}) => {
    return <Form>
        <h4 className={classes.heading}>Document</h4>
        <FormGroup row>
            <Label sm={2} for="title">Title</Label>
            <Col sm={10}>
                <Input type="text" id="title" onFocus={e => e.target.select()} value={design.title}
                       onChange={(event) => {
                           design.title = event.target.value
                           doUpdate()
                       }}/>
            </Col>
        </FormGroup>
    </Form>
})


const RenderTitle = injectSheet(styles)(function ({classes, design, key}) {
    return <Col key={key}>
        <div className={classes.title}>{design.title}</div>
    </Col>
})

widgets.on("edit.*", function ({design, document, layout, editor}) {
    design.title && layout.headerCentre.push(componentAsPrioritisedFunction(RenderTitle, 2))
    design.title !== undefined && editor.tabs.general.content.push(componentAsPrioritisedFunction(RenderTab, 0))
})


