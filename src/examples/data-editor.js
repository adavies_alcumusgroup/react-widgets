import React from "react"
import injectSheet from "react-jss"
import {componentAsPrioritisedFunction, widgets} from "../component"
import AceEditor from "react-ace"
import "brace/mode/json"
import "brace/theme/github"
import {Col, FormGroup, Input, Label} from "reactstrap"

const styles = {
    editor: {
        width: "100%",
        flexGrow: 1,
    },
}

const DataEditor = injectSheet(styles)(function DataEditor({classes, focus: {selectedWidget}, queueUpdate}) {

    return (
        <div className="d-flex flex-column">
            <FormGroup row>
                <Label sm={2}>Label Key</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.series || ""}
                           placeholder="Key for the labels. Defaults to 'name'" onChange={e => {
                        selectedWidget.series = e.target.value
                        queueUpdate()
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Value Key</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.value || ""}
                           placeholder="Key for the numeric value to be displayed. Defaults to 'value'" onChange={e => {
                        selectedWidget.value = e.target.value
                        queueUpdate()
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Data</Label>
                <Col sm={10}>
                    <AceEditor width="100%" height="18em" mode="json" theme="github"
                               value={JSON.stringify(selectedWidget.data, null, 2)}
                               onChange={(e) => {
                                   try {
                                       selectedWidget.data = JSON.parse(e)
                                       queueUpdate()
                                   } catch (e) {
                                       queueUpdate.cancel()
                                   }
                               }}/>
                </Col>
            </FormGroup>
        </div>
    )
})

widgets.on("edit.dashboard", ({focus: {selectedWidget}, editor: {tabs}}) => {
    if (selectedWidget && selectedWidget.data) {
        tabs.data = tabs.data || {title: "Data", content: []}
        tabs.data.content.push(componentAsPrioritisedFunction(DataEditor, 1))
    }
})
