import React from "react"
import {componentAsPrioritisedFunction, useStateFromValueAndForward, widgets} from "../component/index"
import {createDefaultQuestion} from "./edit-questions"
import {Col, FormGroup, Input, Label} from "reactstrap"
import injectSheet from "react-jss"


widgets.on("questions.types", types => {
    types.push({
        name: "Text",
        create: () => createDefaultQuestion({type: "text", question: "Text question"})
    })
})

const style = {
    input: {
        margin: {
            top: 8,
            bottom: 8
        }
    }
}

const Render = injectSheet(style)(({value, setValue, design, classes, readOnly}) => {
    const [text, set] = useStateFromValueAndForward(value, setValue)
    return (
        design.item.question ? <FormGroup key={design.item.id}>
                <Label dangerouslySetInnerHTML={{__html: design.item.question}}/>
                <Input readOnly={readOnly} key={design.item.id} type="text" value={text || ""}
                       onChange={set} placeholder={design.item.placeholder || ""}/>
            </FormGroup>
            : (
                <Input readOnly={readOnly} key={design.item.id} className={classes.input} type="text" value={text || ""}
                       onChange={set} placeholder={design.item.placeholder || ""}/>
            )
    )
})


widgets.on("editor.question.text", ({content, ok}) => {
    ok && content.push(componentAsPrioritisedFunction(Render))
})

const PlaceHolderInfo = ({focus: {selectedQuestion}, doUpdate}) => {
    return <FormGroup row>
        <Label sm={2}>Placeholder</Label>
        <Col sm={10}>
            <Input type="text" value={selectedQuestion.placeholder || ""} onFocus={e => e.target.select()}
                   onChange={event => {
                       selectedQuestion.placeholder = event.target.value
                       doUpdate()
                   }}/>
        </Col>
    </FormGroup>
}
PlaceHolderInfo.priority = 2

widgets.on("edit.*", function ({editor}) {
    editor.focus.selectedQuestion && editor.focus.selectedQuestion.type === "text" && editor.tabs.general.content.push(PlaceHolderInfo)
})
