import React, {useState} from 'react'
import Widgets from "./component/widgets"
import "./examples/dashboard-items"
import "./examples/questions"
import "./examples/title"

import "./examples/list-trash"
import "./examples/list-move"
import "./examples/edit-questions"
import "./examples/text-question"
import "./examples/list-question"
import "./examples/filter-panel"
import "./examples/filter-dashboard-panel"
import "./examples/filter-unanswered"
import "./examples/bar-chart-widget"
import "./examples/pie-chart-widget"
import "./examples/line-chart-widget"
import "./examples/sparkline-chart-widget"
import "./examples/html-widget"
import "./examples/number-widget"
import "./examples/duplicate-list-item"
import "./examples/size-item"
import "./examples/make-readonly"
import "./examples/data-editor"
import dashboardExample from "./examples/example/dashboard.json"
import questionsExample from "./examples/example/questions.json"
import injectSheet from "react-jss"
import {Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap"

let document = JSON.parse(localStorage.getItem("document") || JSON.stringify({}))
let design = JSON.parse(localStorage.getItem("design") || JSON.stringify(questionsExample))

let dashboardDocument = JSON.parse(localStorage.getItem("dashboardDocument") || JSON.stringify({}))
let dashboardDesign = JSON.parse(localStorage.getItem("dashboardDesign") || JSON.stringify(dashboardExample))

const styles = {
    body: {
        flexGrow: 1,
        overflowY: "auto"
    },
    nav: {
        padding: "0.4em",
        "& .nav-item": {
            cursor: "pointer"
        }
    },
    frame: {
        display: "flex",
        flexDirection: "column",
        height: "100%"
    }
}

function Application({classes}) {
    let [mode, setMode] = useState("dashboard")
    let [editable, setEditable] = useState(true)
    let [readonly, setReadOnly] = useState(document.readonly)
    return (
        <div className={classes.frame}>
            <Nav pills className={classes.nav}>
                <NavItem>
                    <NavLink active={mode === "questions"} onClick={() => setMode("questions")}>Questions</NavLink>
                </NavItem>
                <NavItem className="mr-auto">
                    <NavLink active={mode === "dashboard"} onClick={() => setMode("dashboard")}>Dashboard</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink active={editable} onClick={() => setEditable(!editable)}>Editable</NavLink>
                </NavItem>

            </Nav>
            <TabContent activeTab={mode} className={classes.frame}>
                <TabPane tabId="questions" className={classes.frame}>
                    <Widgets readOnly={readonly} setReadOnly={setReadOnly} editable={editable} editorMinHeight={[0.3, 300, 0.9]} editorMaxHeight={[0.3,300,0.9]} className={classes.body} document={document} design={design} updated={() => {
                        localStorage.setItem("design", JSON.stringify(design))
                        localStorage.setItem("document", JSON.stringify(document))
                    }}/>
                </TabPane>
                <TabPane tabId="dashboard" className={classes.frame}>
                    <Widgets editable={editable} editorMinHeight={[0.3, 300, 0.6]} editorMaxHeight={[0.3, 300, 0.6]}
                             type="dashboard" document={dashboardDocument} design={dashboardDesign} updated={() => {
                        localStorage.setItem("dashboardDesign", JSON.stringify(dashboardDesign))
                        localStorage.setItem("dashboardDocument", JSON.stringify(dashboardDocument))
                    }}/>
                </TabPane>
            </TabContent>

        </div>
    )
}

export default injectSheet(styles)(Application)
